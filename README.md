# drf-vue-admin

#### 介绍
一个前后端分离的后端管理系统，drf+vue,此项目目前换处于开发阶段

#### 软件架构
后端采用python的django+drf框架，前端采用vue-elemadmin


#### 安装教程

1.  前端： npm install
2.  后端： pip install -r requirements

#### 使用说明

1.  前端：npm run dev
2.  后端：python manage.py runserver
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1. 感谢醉酒千殇提供的前端模板
