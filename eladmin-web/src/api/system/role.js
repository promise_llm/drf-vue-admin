import request from '@/utils/request'

// 获取所有的Role
export function getAll() {
  return request({
    url: 'system/role/all/',
    method: 'get'
  })
}

export function add(data) {
  return request({
    url: 'system/roles/',
    method: 'post',
    data
  })
}

// 获取用户单条信息
export function get(id) {
  return request({
    url: 'sys/role/retrieve/' + id,
    method: 'get'
  })
}

export function getLevel() {
  return request({
    url: 'roles/level/',
    method: 'get'
  })
}

export function del(ids) {
  return request({
    url: 'system/roles/pk/',
    method: 'delete',
    data: ids
  })
}

// 角色修改
export function edit(data) {
  return request({
    url: 'system/roles/pk/',
    method: 'put',
    data
  })
}

export function editMenu(data) {
  return request({
    url: 'sys/roles/menu/',
    method: 'put',
    data
  })
}

export default { add, edit, del, get, editMenu, getLevel }
