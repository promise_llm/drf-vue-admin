import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'timing/jobs/',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/jobs',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/jobs',
    method: 'put',
    data
  })
}

export function updateIsPause(id) {
  return request({
    url: 'timing/jobs/' + id,
    method: 'put'
  })
}

export function execution(id) {
  return request({
    url: 'api/jobs/exec/' + id,
    method: 'put'
  })
}

// 获取调度任务列表
export function getJobs(data) {
  return request({
    url: '/timing/jobs/',
    method: 'get',
    params: data
  })
}

// 启动/暂停调度任务
export function updateJob(id, data) {
  return request({
    url: `/timing/jobs/${id}/`,
    method: 'put',
    data
  })
}


// 清除所有调度任务
export function deleteJobs() {
  return request({
    url: '/timing/jobs/',
    method: 'delete'
  })
}

// 删除调度任务
export function deleteJob(id) {
  return request({
    url: `/timing/jobs/${id}/`,
    method: 'delete'
  })
}
export default { del, updateIsPause, execution, add, edit }
