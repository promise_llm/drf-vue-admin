import request from '@/utils/request'

// 角色的右侧展示，，菜单编辑的请求
export function getMenusTree(pid) {
  return request({
    url: 'system/menus/?pid=' + pid,
    method: 'get'
  })
}

export function getMenus(params) {
  return request({
    url: 'system/menus/',
    method: 'get',
    params
  })
}

// 菜单编辑
export function getMenuSuperior(ids) {
  const data = Array.isArray(ids) || ids.length === 0 ? ids : Array.of(ids)
  return request({
    url: 'menus/superior/',
    method: 'post',
    data
  })
}

export function getChild(id) {
  return request({
    url: 'system/menus/child?id=' + id,
    method: 'get'
  })
}

// 用户登录获取菜单树
export function buildMenus() {
  return request({
    url: 'system/menu/',
    method: 'get'
  })
}

// 增
export function add(data) {
  return request({
    url: 'system/menu/',
    method: 'post',
    data
  })
}

// 批量删除
export function del(ids) {
  return request({
    url: 'system/menu/pk/',
    method: 'delete',
    data: ids
  })
}

// 改
export function edit(data) {
  return request({
    url: 'system/menu/pk/',
    method: 'put',
    data
  })
}

export default { add, edit, del, getMenusTree, getMenuSuperior, getMenus, getChild }
