import request from '@/utils/request'
import { encrypt } from '@/utils/rsaEncrypt'

export function add(data) {
  return request({
    url: 'system/user/',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'system/user/pk/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'system/user/pk/',
    method: 'put',
    data
  })
}

//用户中心板块
export function editUser(data) {
  return request({
    url: 'users/center/',
    method: 'put',
    data
  })
}

export function updatePass(user) {
  const data = {
    // oldPass: encrypt(user.oldPass),
    // newPass: encrypt(user.newPass)
    oldPass: user.oldPass,
    newPass: user.newPass
  }
  return request({
    url: 'system/password/',
    method: 'post',
    data
  })
}

export function updateEmail(form) {
  const data = {
    password: form.pass,
    email: form.email
  }
  return request({
    // url: 'system/users/updateEmail/' + form.code,
    url: 'system/users/updateEmail/?code=' + form.code,
    method: 'post',
    data
  })
}

export default { add, edit, del }

