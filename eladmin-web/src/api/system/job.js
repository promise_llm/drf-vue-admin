import request from '@/utils/request'

export function getAllJob() {
  const params = {
    page: 1,
    size: 10,
  }
  return request({
    url: 'system/job/',
    method: 'get',
    params
  })
}

export function add(data) {
  return request({
    url: 'system/job/',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'system/job/pk/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'system/job/pk/',
    method: 'put',
    data
  })
}

export default { add, edit, del }
