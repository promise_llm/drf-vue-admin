import request from '@/utils/request'

export function getErrDetail(id) {
  return request({
    url: 'monitar/errorlog/' + id,
    method: 'get'
  })
}

export function delAllError() {
  return request({
    url: 'monitar/error/pk/',
    method: 'delete'
  })
}


// 清空操作日志
export function delAllInfo() {
  return request({
    url: 'monitar/operation/pk/',
    method: 'delete'
  })
}
