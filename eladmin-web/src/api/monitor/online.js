import request from '@/utils/request'

export function del(keys) {
  return request({
    url: 'monitar/online/pk/',
    method: 'delete',
    data: keys
  })
}
