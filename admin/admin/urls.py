"""admin URL Configuration

The `urlpatterns` list routes URLs to view. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function view
    1. Add an import:  from my_app import view
    2. Add a URL to urlpatterns:  path('', view.home, name='home')
Class-based view
    1. Add an import:  from other_app.view import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.views.static import serve
from django.urls import path, include
from django.conf.urls.static import static

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="drf-vue-admin API接口文档平台",  # 必传
        default_version='v1',  # 必传
        description="接口文档",
        terms_of_service="http://api.keyou.site",
    ),
    public=True,
    authentication_classes=[]
)

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/v1/swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('media/<path:path>', serve, {'document_root': settings.MEDIA_ROOT}),
    path('api/v1/', include('user.urls')),
    path('api/v1/', include('system.urls')),
    path('api/v1/', include('CRUDlogs.urls')),
    path('api/v1/', include('monitar.urls')),
    path('api/v1/', include('timing.urls')),

]
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
