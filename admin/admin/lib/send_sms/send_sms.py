import smtplib
from email.mime.text import MIMEText
from email.header import Header
import random


class Mail:
    def __init__(self, receivers):
        # 第三方 SMTP 服务

        self.mail_host = "smtp.qq.com"  # 设置服务器:这个是qq邮箱服务器，直接复制就可以
        self.mail_pass = "wxfqniottywmdhdf"  # 刚才我们获取的授权码
        self.sender = '3067303510@qq.com'  # 你的邮箱地址
        self.receivers = receivers  # 收件人的邮箱地址，可设置为你的QQ邮箱或者其他邮箱，可多个

    def get_code(self):
        code = ''
        for i in range(6):
            code += str(random.randint(0, 9))
        return code

    def send(self, content):

        message = MIMEText(content, 'plain', 'utf-8')

        message['From'] = Header("eladmin后台管理系统", 'utf-8')
        message['To'] = Header(self.receivers, 'utf-8')

        subject = 'eladmin后台管理系统'  # 发送的主题，可自由填写
        message['Subject'] = Header(subject, 'utf-8')
        try:
            smtpObj = smtplib.SMTP_SSL(self.mail_host, 465)
            smtpObj.login(self.sender, self.mail_pass)
            smtpObj.sendmail(self.sender, self.receivers, message.as_string())
            smtpObj.quit()
            return True
        except smtplib.SMTPException as e:
            return False


# if __name__ == '__main__':
#     mail = Mail()
#     mail.send()