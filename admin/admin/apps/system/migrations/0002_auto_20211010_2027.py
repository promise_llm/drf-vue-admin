# Generated by Django 2.2.16 on 2021-10-10 20:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dept',
            name='pid',
            field=models.IntegerField(null=True, verbose_name='父部门id'),
        ),
    ]
