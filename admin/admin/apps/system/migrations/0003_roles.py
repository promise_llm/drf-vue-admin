# Generated by Django 2.2.16 on 2021-10-11 13:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0002_auto_20211010_2027'),
    ]

    operations = [
        migrations.CreateModel(
            name='Roles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name='更新时间')),
                ('create_by', models.CharField(blank=True, max_length=32, null=True, verbose_name='创建者')),
                ('update_by', models.CharField(blank=True, max_length=32, null=True, verbose_name='更新者')),
                ('is_delete', models.BooleanField(default=False, verbose_name='是否删除')),
                ('name', models.CharField(blank=True, max_length=32, null=True, verbose_name='角色名')),
                ('level', models.IntegerField(blank=True, null=True, verbose_name='角色级别')),
                ('description', models.CharField(blank=True, max_length=255, null=True, verbose_name='描述信息')),
                ('data_scope', models.CharField(blank=True, max_length=32, null=True, verbose_name='权限描述')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
