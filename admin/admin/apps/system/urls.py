from rest_framework.routers import SimpleRouter
from django.urls import path, include

from system.views import MenuView, DeptMenuVew, MenuViewSuperior, RoleMenuGetIdList
from system.view.roles import RoleLevelView, RoleView, UpdateRoleMenuView, RetrieveView, GetAllRolesView
from system.view.dept import DeptAllView, DeptSuperiorView
from system.view.job import JobView
from system.view.dict import DictView, DictDetailView
from system.view.user import UserCRUDView, UserCenterPasswordView, UserCenterEmailPssword, UserCenterUpdateInfoView, \
    UserCenterUpdateAvaterView

router = SimpleRouter()
router.register('menu', MenuView)
router.register('roles', RoleView)
router.register('menus', DeptMenuVew)
router.register('dept', DeptAllView)
router.register('job', JobView)
router.register('dict', DictView)
router.register('dictDetail', DictDetailView)
router.register('user', UserCRUDView)
router.register('password', UserCenterPasswordView)
router.register('', UserCenterEmailPssword)


urlpatterns = [
    path('system/', include(router.urls)),
    path('system/role/all/', GetAllRolesView.as_view()),
    path('roles/level/', RoleLevelView.as_view()),
    path('sys/superior/', DeptSuperiorView.as_view()),
    path('menus/superior/', MenuViewSuperior.as_view()),
    path('sys/roles/menu/', UpdateRoleMenuView.as_view()),
    path('system/menus/child', RoleMenuGetIdList.as_view()),         # 角色绑定菜单获取菜单列表
    path('sys/role/retrieve/<int:pk>', RetrieveView.as_view()),         # 角色绑定菜单获取菜单列表
    path('users/center/', UserCenterUpdateInfoView.as_view()),         # 用户中心修改用户配置
    path('users/updateAvatar/', UserCenterUpdateAvaterView.as_view()),         # 用户中心修改用户配置

]


