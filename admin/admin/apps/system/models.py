from django.db import models
from utils.model import BaseModel


class Menu(BaseModel):
    """
    上级菜单id, 子菜单数目， 菜单类型， 菜单标题， 组件名称， 组件， 排序， 图标， 连接地址， 是否外链， 缓存， 隐藏
    权限
    """
    pid = models.ForeignKey('self', verbose_name='父菜单id', on_delete=models.SET_NULL, null=True, blank=True)
    sub_count = models.IntegerField(verbose_name='子菜单数目', null=True, blank=True)
    type = models.IntegerField(verbose_name='菜单类型', blank=True, null=True)
    title = models.CharField(max_length=32, verbose_name='菜单标题', blank=True, null=True, unique=True)
    name = models.CharField(max_length=255, verbose_name='组件名称', blank=True, null=True, unique=True)
    component = models.CharField(max_length=255, verbose_name='组件', blank=True, null=True)
    menu_sort = models.IntegerField(verbose_name='排序', blank=True, null=True)
    icon = models.CharField(max_length=255, blank=True, null=True, verbose_name='图标')
    path = models.CharField(max_length=255, blank=True, null=True, verbose_name='链接地址')
    i_frame = models.BooleanField(default=False, verbose_name='是否外链')
    cache = models.BooleanField(default=False, verbose_name='缓存')
    hidden = models.BooleanField(default=False, verbose_name='是否隐藏')
    permission = models.CharField(max_length=255, verbose_name='权限', blank=True, null=True)
    is_menu = models.BooleanField(default=False, verbose_name='是否是菜单')

    class Meta:
        db_table = 'sys_menu'
        verbose_name = '菜单表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title

class Dept(BaseModel):
    """
    id, 父部门id, 子部门数目, name,
    """
    pid = models.ForeignKey('self', verbose_name='父部门id', null=True, blank=True, on_delete=models.SET_NULL)
    sub_count = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=64, verbose_name='部门名', unique=True)
    enabled = models.BooleanField(default=True, verbose_name='状态')
    dept_sort = models.IntegerField(verbose_name='排序', blank=True, null=True)

    class Meta:
        db_table = 'sys_dept'
        verbose_name = '部门表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

class Roles(BaseModel):
    """
    name, 级别， 描述， 数据权限，
    """
    name = models.CharField(max_length=32, verbose_name='角色名', blank=True, null=True, unique=True)
    level = models.IntegerField(verbose_name='角色级别',blank=True, null=True)
    description = models.CharField(max_length=255, verbose_name='描述信息', blank=True, null=True)
    data_scope = models.CharField(max_length=32, verbose_name='权限描述', blank=True, null=True)

    depts = models.ManyToManyField(to=Dept, db_table='sys_roles_depts', verbose_name='角色和部门的关联表')
    menus = models.ManyToManyField(to=Menu, db_table='sys_roles_menus', verbose_name='角色和菜单的关联表')

    class Meta:
        db_table = 'sys_role'
        verbose_name = '角色表'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.name





class Job(BaseModel):
    """
    岗位名称， 岗位状态， 排序，
    """
    name = models.CharField(max_length=32, verbose_name='岗位名称', blank=True, null=True)
    enabled = models.BooleanField(verbose_name='岗位状态', default=False)
    job_sort = models.IntegerField(verbose_name='排序', blank=True, unique=True)

    class Meta:
        db_table = 'sys_job'
        verbose_name = '岗位表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name



class Dict(BaseModel):
    """
    字典名， 描述，
    """
    name = models.CharField(max_length=32, verbose_name='字典名称', unique=True)
    description = models.CharField(max_length=255, verbose_name='描述', blank=True, null=True)

    class Meta:
        db_table = 'sys_dict'
        verbose_name = '数据字典'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.name


class DictDetail(BaseModel):
    """
    字典id与字典详情一对多， 字典标签， 字典值， 排序，
    """
    dict = models.ForeignKey(to=Dict, on_delete=models.SET_NULL, verbose_name='字典和字典详情的一对一', null=True)
    label = models.CharField(max_length=64, verbose_name='字典标签', blank=True, null=True)
    value = models.CharField(max_length=64, verbose_name='字典值', blank=True, null=True)
    dict_sort = models.IntegerField(verbose_name='排序', blank=True, null=True)

    class Meta:
        db_table = 'sys_dict_detail'
        verbose_name = '字典详情表'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.value