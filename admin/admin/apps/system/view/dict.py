from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin, CreateModelMixin, UpdateModelMixin, DestroyModelMixin
from admin.utils.page import Mypage
from system.serializer.dict import DictSerializer, DictDetailSerializer, CreateDictSerializer, UpdateDictSerializer, \
    CreateDictDetailSerializer, UpdateDictDetailSerializer
from system.models import Dict, DictDetail
from rest_framework.filters import SearchFilter
from admin.utils.filter import MyDictDetailfilter

# 字典
class DictView(GenericViewSet, ListModelMixin, CreateModelMixin, UpdateModelMixin, DestroyModelMixin):
    queryset = Dict.objects.filter(is_delete=False).order_by('id')
    serializer_class = DictSerializer
    pagination_class = Mypage
    filter_backends = [SearchFilter, ]
    search_fields = ['name', ]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object(request)
        serializer = self.get_serializer(instance, data=request.data, partial=partial, context={'request': request})
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_del_object(request)
        for i in instance:
            self.perform_destroy(i)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.update(is_delete=True)

    def get_del_object(self, request):  # 单个删除或批量删除
        id_list = request.data
        dict_list = []
        for id in id_list:
            dict_list.append(Dict.objects.filter(pk=id))
        return dict_list


    def get_object(self, request):
        id = request.data.get('id')
        return Dict.objects.filter(pk=id).first()

    def get_serializer_class(self):
        if self.action == 'list':
            return DictSerializer
        elif self.action == 'create':
            return CreateDictSerializer
        elif self.action == 'update':
            return UpdateDictSerializer





# 字典详情的接口
class DictDetailView(GenericViewSet, ListModelMixin, CreateModelMixin, UpdateModelMixin, DestroyModelMixin):
    queryset = DictDetail.objects.filter(is_delete=False).order_by('id')
    serializer_class = DictDetailSerializer
    filter_backends = [MyDictDetailfilter, ]
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        data = {'results': serializer.data}
        data['count'] = len(data['results'])
        return Response(data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object(request)
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)
    def get_object(self, request):
        id = request.data.get('id')
        return DictDetail.objects.filter(pk=id).first()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_del_object(request)
        for i in instance:
            self.perform_destroy(i)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.update(is_delete=True)

    def get_del_object(self, request):  # 单个删除或批量删除
        id_list = request.data
        detail_list = []
        for id in id_list:
            detail_list.append(DictDetail.objects.filter(pk=id))
        return detail_list

    def get_serializer_class(self):
        if self.action == 'list':
            return DictDetailSerializer
        elif self.action == 'create':
            return CreateDictDetailSerializer
        else:
            return UpdateDictDetailSerializer