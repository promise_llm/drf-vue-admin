from rest_framework import serializers
from system import models


class DictDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DictDetail
        fields = ['dict', 'dictSort', 'id', 'label', 'value']

    dict = serializers.SerializerMethodField()
    def get_dict(self, obj):
        return {
            'id': obj.dict.pk
        }

    dictSort = serializers.SerializerMethodField()
    def get_dictSort(self, obj):
        return obj.dict_sort

class DictSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Dict
        fields = ['createTime', 'description', 'dictDetails', 'id', 'name']

    createTime = serializers.SerializerMethodField()
    def get_createTime(self, obj):
        return obj.create_time

    dictDetails = serializers.SerializerMethodField()
    def get_dictDetails(self, obj):
        dict_list = []
        for i in obj.dictdetail_set.all():
            dict_list.append({
                'dict': {'id': obj.pk},
                'dictSort': i.dict_sort,
                'id': i.pk,
                'label': i.label,
                'value': i.value
            })
        return dict_list


class CreateDictSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Dict
        fields = ['description', 'dictDetails', 'id', 'name']

    dictDetails = serializers.SerializerMethodField()
    def get_dictDetails(self, obj):
        return [ dict.label for dict in obj.dictdetail_set.all()]

    def validate(self, attrs):
        request = self.context.get('request')
        attrs['create_by'] = request.user.username
        return attrs

    def create(self, validated_data):
        return super().create(validated_data)


class UpdateDictSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Dict
        fields = ['createTime', 'description', 'dictDetails', 'id', 'name']

    createTime = serializers.SerializerMethodField()
    def get_createTime(self, obj):
        return obj.create_time

    dictDetails = serializers.SerializerMethodField()
    def get_dictDetails(self, obj):
        return [ dict.label for dict in obj.dictdetail_set.all()]

    def validate(self, attrs):
        request = self.context.get('request')
        attrs['update_by'] = request.user.username
        return attrs

    def update(self, instance, validated_data):
        return super().update(instance, validated_data)


class CreateDictDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DictDetail
        fields = ['dict', 'dictSort', 'id', 'label', 'value']

    dict = serializers.CharField(read_only=True)
    dictSort = serializers.CharField(read_only=True)

    def validate(self, attrs):
        request = self.context.get('request')
        attrs['create_by'] = request.user.username
        attrs['dict_sort'] = request.data.get('dictSort')
        attrs['dict_id'] = request.data.get('dict').get('id')
        return attrs

    def create(self, validated_data):
        dict_detail = models.DictDetail.objects.create(**validated_data)
        return dict_detail


class UpdateDictDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DictDetail
        fields =['dict', 'dictSort', 'id', 'label', 'value']

    dict = serializers.CharField(read_only=True)
    dictSort = serializers.CharField(read_only=True)

    def validate(self, attrs):
        request = self.context.get('request')
        attrs['create_by'] = request.user.username
        attrs['dict_sort'] = request.data.get('dictSort')
        attrs['dict_id'] = request.data.get('dict').get('id')
        return attrs

    def update(self, instance, validated_data):
        return super().update(instance, validated_data)