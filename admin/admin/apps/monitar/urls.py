from rest_framework.routers import SimpleRouter
from django.urls import path, include

from monitar.views import GetOnlineUserView, OperationLogVIew, ErrorLogView, ErrorDetailView
from monitar.view.service import ServiceMonitorAPIView

router = SimpleRouter()
router.register('online', GetOnlineUserView)
router.register('operation', OperationLogVIew)
router.register('error', ErrorLogView)


urlpatterns = [
    path('monitar/', include(router.urls)),
    path('monitar/errorlog/<int:pk>', ErrorDetailView.as_view()),
    path('monitar/service/', ServiceMonitorAPIView.as_view())

]
