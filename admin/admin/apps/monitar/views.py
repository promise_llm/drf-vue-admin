from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin, DestroyModelMixin
from rest_framework.generics import GenericAPIView
from CRUDlogs.models import OperationLog
from monitar.models import ErrorLog, ErrorDetail
from monitar.serializer.operations import OperationLogSerializer
from monitar.serializer.error import ErrorLogSerializer, ErrorDetailLogSerializer
from utils.page import Mypage
from monitar.serializer.online import GetOnlineUserSerializer
from user.models import OnlinUser
from rest_framework.filters import SearchFilter

# 用户中心日志
class GetOnlineUserView(GenericViewSet, ListModelMixin, DestroyModelMixin):
    queryset = OnlinUser.objects.filter(is_delete=False).order_by('id')
    serializer_class = GetOnlineUserSerializer
    pagination_class = Mypage
    filter_backends = [SearchFilter, ]
    search_fields = ['ip', 'address']

    def destroy(self, request, *args, **kwargs):
        instance = self.get_del_object(request)
        for i in instance:
            self.perform_destroy(i)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.update(is_delete=True)

    def get_del_object(self, request):  # 单个删除或批量删除
        del_key_list = request.data
        del_user_list = []
        for del_key in del_key_list:
            del_user_list.append(OnlinUser.objects.filter(key=del_key))
        return del_user_list


# 操作日志
class OperationLogVIew(GenericViewSet, ListModelMixin, DestroyModelMixin):
    queryset = OperationLog.objects.all().order_by('id')
    serializer_class = OperationLogSerializer
    pagination_class = Mypage
    filter_backends = [SearchFilter, ]
    search_fields = ['request_ip', 'request_location', 'request_browser', 'request_os']

    def destroy(self, request, *args, **kwargs):
        OperationLog.objects.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# 错误日志
class ErrorLogView(GenericViewSet, ListModelMixin, DestroyModelMixin):
    queryset = ErrorLog.objects.all().order_by('id')
    serializer_class = ErrorLogSerializer
    pagination_class = Mypage
    filter_backends = [SearchFilter, ]
    search_fields = ['request_ip', 'request_location', 'request_browser', 'request_os']
    def destroy(self, request, *args, **kwargs):
        ErrorLog.objects.all().delete()
        ErrorDetail.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# 获取错误日志详情
class ErrorDetailView(GenericAPIView, ListModelMixin):
    queryset = ErrorDetail.objects.all().order_by('id')
    serializer_class = ErrorDetailLogSerializer
    pagination_class = Mypage

    def get(self, request, pk, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset(pk))
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self, pk):
        return ErrorDetail.objects.filter(pk=pk)
