from rest_framework import serializers
from CRUDlogs.models import OperationLog


class OperationLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = OperationLog
        fields = ['username', 'requestIp', 'address', 'description', 'browser', 'createTime'
                  ,'method', 'params', 'request_os']

    method = serializers.SerializerMethodField(read_only=True)
    def get_method(self, obj):
        return obj.request_method

    params = serializers.SerializerMethodField(read_only=True)
    def get_params(self, obj):
        return obj.request_body

    username = serializers.SerializerMethodField(read_only=True)
    def get_username(self, obj):
        return obj.creator.username if obj.creator else 'Anymoreuser'

    requestIp = serializers.SerializerMethodField(read_only=True)
    def get_requestIp(self, obj):
        return obj.request_ip

    address = serializers.SerializerMethodField(read_only=True)
    def get_address(self, obj):
        return obj.request_location

    browser = serializers.SerializerMethodField(read_only=True)
    def get_browser(self, obj):
        return obj.request_browser

    createTime = serializers.SerializerMethodField(read_only=True)
    def get_createTime(self, obj):
        return obj.create_time