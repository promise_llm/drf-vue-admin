from rest_framework import serializers
from monitar.models import ErrorLog, ErrorDetail

class ErrorLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ErrorLog
        fields = ['username', 'requestIp', 'address', 'description', 'browser', 'createTime', 'id',
                  'method', 'params']

    method = serializers.SerializerMethodField(read_only=True)
    def get_method(self, obj):
        return obj.request_method

    params = serializers.SerializerMethodField(read_only=True)
    def get_params(self, obj):
        return obj.request_body

    username = serializers.SerializerMethodField(read_only=True)
    def get_username(self, obj):
        return obj.creator.username if obj.creator else 'AnymoreUser'

    requestIp = serializers.SerializerMethodField(read_only=True)
    def get_requestIp(self, obj):
        return obj.request_ip

    address = serializers.SerializerMethodField(read_only=True)
    def get_address(self, obj):
        return obj.request_location

    browser = serializers.SerializerMethodField(read_only=True)
    def get_browser(self, obj):
        return obj.request_browser

    createTime = serializers.SerializerMethodField(read_only=True)
    def get_createTime(self, obj):
        return obj.create_time

    id = serializers.SerializerMethodField(read_only=True)
    def get_id(self, obj):
        return obj.detail.pk


class ErrorDetailLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ErrorDetail
        fields = ['exception']

    exception = serializers.SerializerMethodField(read_only=True)
    def get_exception(self, obj):
        return obj.detail