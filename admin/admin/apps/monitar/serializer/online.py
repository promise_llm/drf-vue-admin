from rest_framework import serializers
from user.models import OnlinUser



class GetOnlineUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = OnlinUser
        fields = ['address', 'browser', 'dept', 'ip', 'key', 'loginTime', 'nickName', 'userName']

    loginTime = serializers.SerializerMethodField(read_only=True)
    def get_loginTime(self, obj):
        return obj.create_time

    nickName = serializers.SerializerMethodField(read_only=True)
    def get_nickName(self, obj):
        return obj.user.nick_name

    userName = serializers.SerializerMethodField(read_only=True)
    def get_userName(self, obj):
        return obj.user.username

    dept = serializers.SerializerMethodField(read_only=True)
    def get_dept(self, obj):
        return obj.user.dept.name