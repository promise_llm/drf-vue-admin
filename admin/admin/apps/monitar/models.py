from django.db import models
from django.conf import settings
from utils.model import BaseModel


class ErrorDetail(models.Model):
    """这里可以扩写字段"""
    detail = models.TextField(verbose_name='错误详情', blank=True, null=True)

    class Meta:
        db_table = 'admin_error_detail_log'
        verbose_name = '错误日志详情表'
        verbose_name_plural = verbose_name


class ErrorLog(BaseModel):
    request_path = models.CharField(max_length=400, verbose_name="请求地址", null=True, blank=True)
    request_body = models.TextField(verbose_name="请求参数", null=True, blank=True)
    request_method = models.CharField(max_length=64, verbose_name="请求方式", null=True, blank=True)
    request_msg = models.TextField(verbose_name="操作说明", null=True, blank=True)
    request_ip = models.CharField(max_length=32, verbose_name="请求ip地址", null=True, blank=True)
    request_browser = models.CharField(max_length=64, verbose_name="请求浏览器", null=True, blank=True)
    request_location = models.CharField(max_length=64, verbose_name="操作地点", null=True, blank=True)
    request_os = models.CharField(max_length=64, verbose_name="操作系统", null=True, blank=True)
    description = models.CharField(max_length=255, blank=True, null=True)  # 描述
    creator = models.ForeignKey(to=settings.AUTH_USER_MODEL, related_query_name='creator_query', null=True,
                                verbose_name='创建者', on_delete=models.SET_NULL, db_constraint=False)  # 创建者
    modifier = models.CharField(max_length=32, blank=True, null=True)
    dept_belong_id = models.CharField(max_length=64, verbose_name="数据归属部门", null=True, blank=True)
    detail = models.OneToOneField(to=ErrorDetail, on_delete=models.SET_NULL, null=True)

    class Meta:
        db_table = 'admin_error_logs'
        verbose_name = '错误日志'
        verbose_name_plural = verbose_name


class Server(BaseModel):
    sys_os = models.CharField(max_length=64, verbose_name='操作系统', blank=True, null=True)
    sys_start_time = models.CharField(max_length=64, verbose_name='服务开启时间', blank=True, null=True)
    sys_ip = models.CharField(max_length=32, verbose_name='ip地址', blank=True, null=True)
    cpu_name = models.CharField(max_length=128, verbose_name='cpu名字', blank=True, null=True)
    cpu_package = models.IntegerField(verbose_name='物理cpu个数', blank=True, null=True)
    cpu_core = models.CharField(max_length=32, verbose_name='物理核心数', blank=True, null=True)
    cpu_core_num = models.IntegerField(verbose_name='物理核心个数', blank=True, null=True)
    cpu_logic = models.IntegerField(verbose_name='逻辑cpu个数', blank=True, null=True)
    cpu_used = models.CharField(max_length=32, verbose_name='已使用的cpu', blank=True, null=True)
    cpu_idle = models.CharField(max_length=32, verbose_name='剩余的cpu', blank=True, null=True)
    memory_total = models.CharField(max_length=32, verbose_name='内存总量', blank=True, null=True)
    memory_available = models.CharField(max_length=32, verbose_name='可使用的内存', blank=True, null=True)
    memory_used = models.CharField(max_length=32, verbose_name='已使用的内存', blank=True, null=True)
    memory_usageRate = models.CharField(max_length=32, verbose_name='未使用的内存', blank=True, null=True)
    swap_total = models.CharField(max_length=32, verbose_name='swap分区总量', blank=True, null=True)
    swap_used = models.CharField(max_length=32, verbose_name='已使用的swap总量', blank=True, null=True)
    swap_available = models.CharField(max_length=32, verbose_name='可使用的swap', blank=True, null=True)
    swap_usageRate = models.CharField(max_length=32, verbose_name='未使用的swap总量', blank=True, null=True)
    disk_total = models.CharField(max_length=32, verbose_name='disk分区总量', null=True, blank=True)
    disk_used = models.CharField(max_length=32, verbose_name='已使用的disk总量', blank=True, null=True)
    disk_available = models.CharField(max_length=32, verbose_name='可使用的disk', blank=True, null=True)
    disk_usageRate = models.CharField(max_length=32, verbose_name='未使用的disk总量', blank=True, null=True)

    class Meta:
        db_table = 'admin_server'
        verbose_name = '服务器信息'
        verbose_name_plural = verbose_name