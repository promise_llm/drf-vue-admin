from rest_framework.routers import SimpleRouter
from django.urls import path, include

from timing.views import JobsListCreateAPIView, JobFunctionsListAPIView, JobUpdateDestroyAPIView, \
    JobExecutionsListAPIView

router = SimpleRouter()

urlpatterns = [
    path('timing/jobs/', JobsListCreateAPIView.as_view()),
    path('jobs/functions/', JobFunctionsListAPIView.as_view()),  # 调度函数
    path('timing/jobs/<uuid:pk>/', JobUpdateDestroyAPIView.as_view()),  # 调度任务启动/恢复,删除
    path('timing/jobs/executions/', JobExecutionsListAPIView.as_view()),  # 调度任务执行过程记录
]
