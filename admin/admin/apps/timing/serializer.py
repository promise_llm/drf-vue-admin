import uuid
from inspect import getmembers, isfunction
from apscheduler.triggers.cron import CronTrigger
from django_apscheduler.models import DjangoJobExecution,DjangoJob
from rest_framework import serializers
from timing.jobs.run import scheduler
from timing.jobs import tasks


class JobFunctionsSerializer(serializers.Serializer):
    """任务调度函数列表序列化器"""
    name = serializers.SerializerMethodField(help_text='任务函数名称')
    desc = serializers.SerializerMethodField(help_text='任务函数描述')

    def get_name(self, obj):
        return obj[0]

    def get_desc(self, obj):
        return obj[1].__doc__    # 输出调度函数的注释


class JobsListSerializer(serializers.Serializer):
    """任务列表序列化器"""
    id = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    desc = serializers.SerializerMethodField()
    next_run_time = serializers.SerializerMethodField()
    cron = serializers.SerializerMethodField()

    def get_id(self, obj):
        return obj.id

    def get_name(self, obj):
        return obj.name

    def get_desc(self, obj):
        return tasks.__dict__.get(obj.name).__doc__

    def get_next_run_time(self, obj):
        # job = DjangoJob.objects.values('next_run_time').filter(pk=obj.id)
        # print(job)
        return obj.next_run_time.strftime('%Y-%m-%d %H:%M:%S') if obj.next_run_time else obj.next_run_time

    def get_cron(self, obj):
        def get_cron(name):
            ob = None
            for field in obj.trigger.fields:
                if field.name == name:
                    ob = field
            return ','.join([str(e) for e in ob.expressions])

        return f'{get_cron("minute")} {get_cron("hour")} {get_cron("day")} {get_cron("month")} {get_cron("day_of_week")}'


class JobCreateSerializer(serializers.Serializer):
    """任务新增序列化器"""

    id = serializers.SerializerMethodField()
    desc = serializers.SerializerMethodField()
    next_run_time = serializers.SerializerMethodField()
    name = serializers.CharField(required=True, error_messages={'required': '函数名称为必传项'})
    cron = serializers.CharField(required=True, write_only=True, error_messages={'required': 'cron表达式为必传项'})

    def get_name(self, obj):
        return obj.jobName

    def get_cron(self, obj):
        return obj.cronExpression


    def get_id(self, obj):
        return obj.id

    def get_desc(self, obj):
        return tasks.__dict__.get(obj.name).__doc__

    def get_next_run_time(self, obj):
        return obj.next_run_time.strftime('%Y-%m-%d %H:%M:%S') if obj.next_run_time else obj.next_run_time

    def validate(self, attrs):
        name = attrs.get('name')
        cron = attrs.get('cron')
        job_function = None
        for obj in getmembers(tasks):
            if isfunction(obj[1]) and obj[0] == name:
                """
                obj[1]: 拿到调度函数
                obj[0]: 函数名
                """
                job_function = obj[1]
                attrs['job_function'] = job_function
                break
        if job_function is None:
            raise serializers.ValidationError('调度任务函数不存在')
        try:
            trigger = CronTrigger.from_crontab(cron)
            # https://blog.csdn.net/abc_soul/article/details/88875643
            attrs['trigger'] = trigger
        except ValueError:
            raise serializers.ValidationError('cron表达式格式错误')
        return attrs

    def get_cron(self, obj):
        def get_cron(name):
            ob = None
            for field in obj.trigger.fields:
                print(obj.trigger.fields)
                if field.name == name:
                    ob = field
            return ','.join([str(e) for e in ob.expressions])
        # print('时间', f'{get_cron("minute")} {get_cron("hour")} {get_cron("day")} {get_cron("month")} {get_cron("day_of_week")}')
        return f'{get_cron("minute")} {get_cron("hour")} {get_cron("day")} {get_cron("month")} {get_cron("day_of_week")}'

    def create(self, validated_data):
        while True:
            job_id = str(uuid.uuid1())
            if scheduler.get_job(job_id):
                continue
            else:
                break
        scheduler.add_job(validated_data.get('job_function'),     # 添加任务
                          args=[],
                          kwargs=None,
                          trigger=validated_data.get('trigger'),   # * */12 1-2 * *
                          id=job_id,
                          max_instances=1,                         # 允许最大并发
                          replace_existing=True,                   # 用相同的`id`替换现有作业``
                          misfire_grace_time=10                    # 被执行时间和设定时间的最大差值
                          )
        return scheduler.get_job(job_id)                           # 返回当前id的任务



class JobUpdateSerializer(serializers.Serializer):
    status = serializers.BooleanField(required=True, write_only=True)

    def validate(self, attrs):
        status = attrs.get('status')
        if status is None:
            raise serializers.ValidationError('该参数是必穿项')
        if not isinstance(status, bool):
            raise serializers.ValidationError('该参数必须是bool类型')
        return attrs

    def save(self, **kwargs):
        if self.validated_data.get('status'):
            self.instance.resume()    # 启动任务
        else:
            self.instance.pause()     # 暂停任务
        return self.instance



class JobExecutionsSerializer(serializers.ModelSerializer):
    """任务执行历史记录序列化器"""
    run_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S', read_only=True)

    class Meta:
        model = DjangoJobExecution
        fields = '__all__'