
import os
import time

from django.conf import settings


def mysql():
    """
    mysql数据库备份
    """
    db_user = settings.DB_USER       # 用户名
    db_pwd = settings.DB_PWD         # 密码
    db_name = settings.DB_NAME       # 库名
    backup_dir = os.path.join(settings.BASE_DIR, 'backup', 'mysql')
    if not os.path.exists(backup_dir):
        os.makedirs(backup_dir)
    time_strf = time.strftime('%Y-%m-%d_%H-%M-%S')
    sql_file_path = os.path.join(backup_dir, time_strf + '.sql')
    sql_cmd = f'mysqldump -u{db_user} -p{db_pwd} {db_name} > {sql_file_path}'
    os.system(sql_cmd)
