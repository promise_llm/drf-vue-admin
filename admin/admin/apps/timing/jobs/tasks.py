
from timing.jobs.decorators import single_task
from .warehouse import backup,code

@single_task('database_backup')
def database_backup():
    """数据库备份"""
    backup.mysql()

@single_task('delete_code_database')
def delete_code_database():
    """
    定时清理验证码数据库
    :return:
    """
    code.del_code()

