from rest_framework import serializers
from CRUDlogs import models


class UserCenterCRUDLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OperationLog
        fields = ['description', 'requestIp', 'address', 'browser', 'os', 'createTime']

    createTime = serializers.SerializerMethodField()
    def get_createTime(self, obj):
        return obj.create_time
    description = serializers.SerializerMethodField()
    def get_description(self, obj):
        return obj.request_method
    requestIp = serializers.SerializerMethodField()
    def get_requestIp(self, obj):
        return obj.request_ip

    address = serializers.SerializerMethodField()
    def get_address(self, obj):
        return obj.request_location

    browser = serializers.SerializerMethodField()
    def get_browser(self, obj):
        return obj.request_browser

    os = serializers.SerializerMethodField()
    def get_os(self, obj):
        return obj.request_os