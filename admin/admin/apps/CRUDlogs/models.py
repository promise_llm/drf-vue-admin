from django.db import models
from utils.model import BaseModel
from django.conf import settings
class CoreModel(models.Model):
    """
    核心标准抽象模型模型,可直接继承使用
    增加审计字段, 覆盖字段时, 字段名称请勿修改, 必须统一审计字段名称
    """
    description = models.CharField(max_length=255, blank=True, null=True)  # 描述
    creator = models.ForeignKey(to=settings.AUTH_USER_MODEL, related_query_name='creator_query', null=True,
                                verbose_name='创建者', on_delete=models.SET_NULL, db_constraint=False)  # 创建者
    modifier = models.CharField(max_length=32, blank=True, null=True)  # 修改者
    dept_belong_id = models.CharField(max_length=64, verbose_name="数据归属部门", null=True, blank=True)
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间', blank=True, null=True)
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间', blank=True, null=True)
    class Meta:
        abstract = True
        verbose_name = '核心模型'
        verbose_name_plural = verbose_name


class OperationLog(CoreModel):
    request_modular = models.CharField(max_length=64, verbose_name="请求模块", null=True, blank=True)
    request_path = models.CharField(max_length=400, verbose_name="请求地址", null=True, blank=True)
    request_body = models.TextField(verbose_name="请求参数", null=True, blank=True)
    request_method = models.CharField(max_length=64, verbose_name="请求方式", null=True, blank=True)
    request_msg = models.TextField(verbose_name="操作说明", null=True, blank=True)
    request_ip = models.CharField(max_length=32, verbose_name="请求ip地址", null=True, blank=True)
    request_browser = models.CharField(max_length=64, verbose_name="请求浏览器", null=True, blank=True)
    response_code = models.CharField(max_length=32, verbose_name="响应状态码", null=True, blank=True)
    request_location = models.CharField(max_length=64, verbose_name="操作地点", null=True, blank=True)
    request_os = models.CharField(max_length=64, verbose_name="操作系统", null=True, blank=True)
    json_result = models.TextField(verbose_name="返回信息", null=True, blank=True)
    status = models.BooleanField(default=False, verbose_name="响应状态")

    class Meta:
        db_table = 'admin_operation_logs'
        verbose_name = '操作日志'
        verbose_name_plural = verbose_name

    def __str__(self):
        return f"{self.request_msg}[{self.request_modular}]"
