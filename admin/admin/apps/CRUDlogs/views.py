
from CRUDlogs import models
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import ListModelMixin
from utils.page import Mypage
from CRUDlogs.serializer import UserCenterCRUDLogSerializer


class UserCenterCRUDLogView(GenericViewSet, ListModelMixin):
    queryset = models.OperationLog.objects.all()
    serializer_class = UserCenterCRUDLogSerializer
    pagination_class = Mypage
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset(request))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            print(serializer)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
    def get_queryset(self, request):
        return models.OperationLog.objects.filter(creator=request.user)


