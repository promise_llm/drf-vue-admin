from rest_framework.routers import SimpleRouter
from django.urls import path, include
from CRUDlogs.views import UserCenterCRUDLogView

router = SimpleRouter()
router.register('logs', UserCenterCRUDLogView)    # 用户中心的操作日志


urlpatterns = [
    path('crud/', include(router.urls))
]
