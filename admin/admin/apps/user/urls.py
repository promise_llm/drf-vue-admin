from rest_framework.routers import SimpleRouter
from django.urls import path, include
from .views import CaptchaView, UserView, UserInfoView, GetUserView, LogoutView, DownloadUserView

router = SimpleRouter()
router.register('login', UserView)
router.register('info', UserInfoView)
router.register('users', GetUserView)


urlpatterns = [
    path('captcha/', include('captcha.urls')),
    path('auth/code/', CaptchaView.as_view()),
    path('auth/', include(router.urls)),
    path('auth/logout/', LogoutView.as_view()),
    path('auth/users/download/', DownloadUserView.as_view()),
]
