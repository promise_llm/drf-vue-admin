from django.contrib.auth.models import AbstractUser
from django.db import models
from system.models import Dept, Job, Roles
from utils.model import BaseModel


class UserInfo(AbstractUser):
    """
    id, 和部门是一对多，昵称，性别，手机号，头像，密码， 状态， 创建者，更新者，创建时间，更新时间，修改密码时间
    """
    nick_name = models.CharField(max_length=32, verbose_name='用户昵称', blank=True, null=True, unique=True)
    gender = models.CharField(max_length=16, verbose_name='性别', blank=True, null=True)
    phone = models.CharField(max_length=11, verbose_name='电话号码', blank=True, null=True, unique=True)
    avatar_path = models.ImageField(upload_to='avatar/%Y/%m', default='avatar/.jpeg', verbose_name='头像')
    enabled = models.BooleanField(default=True, verbose_name='是否启用?状态：1启用、0禁用')
    create_by = models.CharField(max_length=32, verbose_name='创建者', blank=True, null=True)
    update_by = models.CharField(max_length=32, verbose_name='更新者', blank=True, null=True)
    pwd_reset_time = models.DateTimeField(auto_now_add=True, verbose_name='修改密码的时间')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
    is_delete = models.BooleanField(default=False, verbose_name='是否删除')
    is_login = models.BooleanField(default=False, verbose_name='是否登录')

    dept = models.ForeignKey(to=Dept, on_delete=models.SET_NULL, verbose_name='部门名字', null=True)

    job = models.ManyToManyField(to=Job, verbose_name='用户和岗位的关联表', db_table='sys_users_jobs')
    roles = models.ManyToManyField(to=Roles, verbose_name='用户和角色的关联表', db_table='sys_users_roles')
    class Meta:
        db_table = 'sys_user'
        verbose_name = '用户表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.username





class OnlinUser(BaseModel):
    address = models.CharField(max_length=64, verbose_name='在线用户登录地址', blank=True, null=True)
    browser = models.CharField(max_length=128, verbose_name='浏览器', blank=True, null=True)
    ip = models.CharField(max_length=64, verbose_name='用户登录ip', blank=True, null=True)
    key = models.CharField(max_length=255, verbose_name='用户登录把token存', blank=True, null=True)
    user = models.ForeignKey(to=UserInfo, on_delete=models.SET_NULL, verbose_name='和用户的一对多', null=True)

    class Meta:
        db_table = 'sys_online_user'
        verbose_name = '在线用户的记录表'
        verbose_name_plural = verbose_name
