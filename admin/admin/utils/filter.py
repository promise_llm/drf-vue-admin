from rest_framework.filters import BaseFilterBackend

class MyDictDetailfilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        name = request.GET.get('dictName')
        if name:
            queryset = queryset.filter(dict__name = name)
            return queryset
        else:
            return queryset

class MyUserFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        dept_id = request.GET.get('deptId')
        if dept_id:
            queryset = queryset.filter(dept__id = dept_id)
            return queryset
        else:
            return queryset


class MyDeptFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        name= request.GET.get('name')
        print(name)
        if name:
            queryset = queryset.filter(name=name)
            print(queryset)
            return queryset
        else:
            return queryset